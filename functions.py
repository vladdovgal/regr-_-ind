import numpy as np
from data import almost_zero

def log(x):
    return np.log(x) if x !=0 else np.log(almost_zero)